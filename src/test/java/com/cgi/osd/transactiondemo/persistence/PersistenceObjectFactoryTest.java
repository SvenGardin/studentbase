package com.cgi.osd.transactiondemo.persistence;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for testing object conversions in the persistence layer.
 *
 */
@RunWith(Arquillian.class)
public class PersistenceObjectFactoryTest {

    @Deployment
    public static Archive<WebArchive> createTestArchive() {
	final WebArchive archive = ShrinkWrap.create(WebArchive.class, "persistence-object-factory-test.war");
	archive.addPackages(true, "com.cgi.osd.transactiondemo.logic");
	archive.addPackages(true, "com.cgi.osd.transactiondemo.persistence");
	archive.addPackages(true, "com.cgi.osd.transactiondemo.util");
	archive.addClasses(PersistenceMangerBeanMockup.class, PersistenceOperationException.class);
	archive.deleteClasses(PersistenceManagerTest.class);
	archive.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
	archive.addAsWebInfResource("test-ds.xml");
	archive.addAsWebInfResource("test-beans.xml", "beans.xml");
	return archive;
    }

    @Inject
    private PersistenceManager persistenceManager;

    @Test
    public void testMockFreeSeats() throws PersistenceOperationException {
	final List<SeatDO> seats = this.persistenceManager.getAllSeats();
	assertEquals(100, seats.size());
	for (int i = 0; i < seats.size(); i++) {
	    final SeatDO seat = seats.get(i);
	    assertEquals(i, seat.getSeatNumber());
	}
    }

}
